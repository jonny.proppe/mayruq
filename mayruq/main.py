import numpy as np
import dill as pkl
import time
import multiprocessing
from   scipy.stats import t as student_t
from   scipy.optimize import basinhopping
from   joblib import Parallel, delayed

#==============================================================================================================#

class Attributes:

    def __init__(self):

        pass

#==============================================================================================================#

class ReactionIndex:
    
    def __init__(self, rxns):
        
        self.nuc  = rxns['index']['nuc']
        self.elec = rxns['index']['elec']

#==============================================================================================================#

class Reactions:
    
    def __init__(self, path_to_rxns='./rxns.pkl'):
        
        rxns = pkl.load(open(path_to_rxns, 'rb'))
        
        self.k_exp  = rxns['k_exp']
        self.status = rxns['status']
        self.index  = ReactionIndex(rxns)
        self.n      = len(rxns['k_exp'])
        
        self._check_rate()
        
    #----------------------------------------------------------------------------------------------------------#
    
    def _check_rate(self):
        
        for r in range(self.n):
            if np.log10(self.k_exp[r]) > 8:
                self.status[r] = 'tf'

#==============================================================================================================#

class Nucleophiles:

    def __init__(self, species):

        self.index        = species['nuc']['index']
        self.sN0          = species['nuc']['sN0']
        self.N0           = species['nuc']['N0']
        self.index_to_sN0 = species['nuc']['index_to_sN0']
        self.index_to_N0  = species['nuc']['index_to_N0']
        self.anchor       = species['nuc']['anchor']
        self.fixed        = species['nuc']['fixed']
        self.n            = len(species['nuc']['index'])

#==============================================================================================================#

class Electrophiles:
    
    def __init__(self, species):
        
        self.index       = species['elec']['index']
        self.E0          = species['elec']['E0']
        self.index_to_E0 = species['elec']['index_to_E0']
        self.anchor      = species['elec']['anchor']
        self.fixed       = species['elec']['fixed']
        self.n           = len(species['elec']['index'])

#==============================================================================================================#

class Species:

    def __init__(self, path_to_species='./species.pkl'):

        species = pkl.load(open(path_to_species, 'rb'))

        self.nuc  = Nucleophiles(species)
        self.elec = Electrophiles(species)

#==============================================================================================================#

class MayrUQ:
    
    def __init__(self, path_to_rxns='./rxns.pkl', path_to_species='./species.pkl', min_n_nuc=3, min_n_elec=2, accept_ap=False):
        
        species = Species(path_to_species=path_to_species)
        
        self.rxns   = Reactions(path_to_rxns=path_to_rxns)
        self.nuc    = species.nuc
        self.elec   = species.elec
        self.result = []
        self.latest = None
        
        if self.nuc.anchor in self.nuc.fixed:
            raise Exception("Anchor nucleophile must not be a member of self.nuc.fixed.")
        if self.elec.anchor in self.elec.fixed:
            raise Exception("Anchor electrophile must not be a member of self.elec.fixed.")
        
        if accept_ap is True:
            for r in range(self.rxns.n):
                if self.rxns.status[r] == 'ap':
                    self.rxns.status[r] = 'ok'

        self._apply_xNyE_rule(min_n_nuc=min_n_nuc, min_n_elec=min_n_elec)
        self._check_connectivity()
        
        self.x0 = self.get_x0()
        
    #----------------------------------------------------------------------------------------------------------#
        
    def _apply_xNyE_rule(self, min_n_nuc=3, min_n_elec=2):
        
        self.nuc.removed  = []
        self.elec.removed = []
        
        novr = self.count('ok') # novr : number of valid reactions
        flag = False
        
        while not flag:
        
            for i in self.nuc.index:
                
                idx = np.where(self.rxns.index.nuc == i)[0]
                
                if i in self.nuc.fixed:
                    for r in idx:
                        if self.rxns.index.elec[r] in self.elec.fixed or self.rxns.index.elec[r] == self.elec.anchor:
                            if self.rxns.status[r] == 'ok':
                                self.rxns.status[r] = 'xy'
                    continue
                
                if i == self.nuc.anchor:
                    min_noo = min_n_nuc - 1 # min_noo : minimum number of occurrences
                else:
                    min_noo = min_n_nuc
                
                noo = (self.rxns.status[idx] == 'ok').sum() # noo : number of occurrences
                
                if noo < min_noo:
                    
                    if not i in self.nuc.removed:
                        self.nuc.removed.append(i)
                    
                    for r in idx:
                        if self.rxns.status[r] == 'ok':
                            self.rxns.status[r] = 'xy'
                            
            for j in self.elec.index:
                
                if j == self.elec.anchor:
                    continue
                    
                idx = np.where(self.rxns.index.elec == j)[0]

                if j in self.elec.fixed:
                    for r in idx:
                        if self.rxns.index.nuc[r] in self.nuc.fixed:
                            if self.rxns.status[r] == 'ok':
                                self.rxns.status[r] = 'xy'
                    continue

                noo = (self.rxns.status[idx] == 'ok').sum()
                
                if noo < min_n_elec:
                    
                    if not j in self.elec.removed:
                        self.elec.removed.append(j)
                    
                    for r in idx:
                        if self.rxns.status[r] == 'ok':
                            self.rxns.status[r] = 'xy'
                            
            if novr != self.count('ok'):
                novr = self.count('ok')
            else:
                flag = True
                
    #----------------------------------------------------------------------------------------------------------#
    
    def _check_connectivity(self):

        if (self.rxns.status[self.rxns.index.nuc == self.nuc.anchor] == 'ok').sum() == 0:
            raise Exception("The anchor nucleophile does not occur in any valid reaction. DO NOT optimize reactivity parameters.")    
                
        if (self.rxns.status[self.rxns.index.elec == self.elec.anchor] == 'ok').sum() == 0:
            raise Exception("The anchor electrophile does not occur in any valid reaction. DO NOT optimize reactivity parameters.")
            
        linked_rxns = [np.where(self.rxns.status == 'ok')[0][0]]
        finished    = False
        
        for id_ in linked_rxns:
            for r in range(self.rxns.n):  
                if self.rxns.status[r] == 'ok':
                    if not r in linked_rxns:
                        if self.rxns.index.nuc[id_] == self.rxns.index.nuc[r] or self.rxns.index.elec[id_] == self.rxns.index.elec[r]:
                        
                            linked_rxns.append(r)
 
        if not len(linked_rxns) == self.count('ok'):
            raise Exception("Disconnected reactions were found. DO NOT optimize reactivity parameters.")
                
    #----------------------------------------------------------------------------------------------------------#
    
    def count(self, status):
        
        return (self.rxns.status == status).sum()
                
    #----------------------------------------------------------------------------------------------------------#

    def get_x0(self, free_only=True):
        
        if free_only is False:
            return np.concatenate((self.nuc.sN0, 
                                   self.nuc.N0, 
                                   self.elec.E0))
        
        elif free_only is True:
            
            x0 = []
            
            for i in self.nuc.index:
                if i == self.nuc.anchor or i in self.nuc.fixed or i in self.nuc.removed:
                    continue
                else:
                    x0.append(self.nuc.index_to_sN0[i])
                    
            for i in self.nuc.index:
                if i in self.nuc.fixed or i in self.nuc.removed:
                    continue
                else:
                    x0.append(self.nuc.index_to_N0[i])
                
            for j in self.elec.index:
                if j == self.elec.anchor or j in self.elec.fixed or j in self.elec.removed:
                    continue
                else:
                    x0.append(self.elec.index_to_E0[j])
                
            return np.array(x0)
        
    #----------------------------------------------------------------------------------------------------------#
        
    def make_sparse(self, x):
        
        if len(x) != len(self.x0):
            raise Exception("Number of elements in x does not equal len(self.get_x0(free_only=True)).")
        
        x_sparse = self.get_x0(free_only=False)
        
        if len(x.shape) == 2:
            x_sparse = np.repeat(x_sparse.reshape(-1, 1), x.shape[1], axis=1)
        
        count = 0
        
        for i in range(self.nuc.n):
            idN = self.nuc.index[i]
            if idN == self.nuc.anchor or idN in self.nuc.fixed or idN in self.nuc.removed:
                continue
            else:
                x_sparse[i]  = x[count]
                count       += 1
            
        for i in range(self.nuc.n):
            idN = self.nuc.index[i]
            if idN in self.nuc.fixed or idN in self.nuc.removed:
                continue
            else:
                x_sparse[self.nuc.n+i]  = x[count]
                count                  += 1
            
        for j in range(self.elec.n):
            idE = self.elec.index[j]
            if idE == self.elec.anchor or idE in self.elec.fixed or idE in self.elec.removed:
                continue
            else:
                x_sparse[2*self.nuc.n+j]  = x[count]
                count                    += 1
                
        return x_sparse
    
    #----------------------------------------------------------------------------------------------------------#
    
    def get_error(self, x, weighting=False, bootstrapping=False, detailed=False):

        x_sparse = self.make_sparse(x)
        error    = []
        
        for r in range(self.rxns.n):
            
            if self.rxns.status[r] != 'ok':
                continue
                
            idN      = np.where(self.nuc.index == self.rxns.index.nuc[r])[0].item()
            idE      = np.where(self.elec.index == self.rxns.index.elec[r])[0].item()
            sN       = x_sparse[idN]
            N        = x_sparse[idN+self.nuc.n]
            E        = x_sparse[idE+2*self.nuc.n]
            lgk_calc = sN * (N + E)
            lgk_exp  = np.log10(self.rxns.k_exp[r])
            
            error.append(lgk_exp - lgk_calc)
            
        error = np.array(error)
        
        if detailed is False:
            if weighting is True and hasattr(self.rxns, 'weights'):
                weights = self.rxns.weights.copy()
                if bootstrapping is True and hasattr(self.rxns, 'pop'):
                    weights *= self.rxns.pop
                weights /= weights.sum()
                return np.sqrt(weights.dot(error**2))
            else:
                if bootstrapping is True and hasattr(self.rxns, 'pop'):
                    return np.sqrt(self.rxns.pop.dot(error**2))
                else:
                    return np.sqrt(np.mean(error**2))
        
        if detailed is True:
            return error
        
    #----------------------------------------------------------------------------------------------------------#
    
    def set_species_error(self, x):
        
        self.nuc.error  = {}
        self.elec.error = {}
        
        x_sparse = self.make_sparse(x)
        
        for i in self.nuc.index:
            if not (i in self.nuc.removed or i in self.nuc.fixed):
                self.nuc.error[i] = []
            
        for j in self.elec.index:
            if not (j in self.elec.removed or j in self.elec.fixed or j == self.elec.anchor):
                self.elec.error[j] = []
        
        for r in range(self.rxns.n):
            
            if self.rxns.status[r] != 'ok':
                continue

            idN      = np.where(self.nuc.index == self.rxns.index.nuc[r])[0].item()
            idE      = np.where(self.elec.index == self.rxns.index.elec[r])[0].item()
            sN       = x_sparse[idN]
            N        = x_sparse[idN+self.nuc.n]
            E        = x_sparse[idE+2*self.nuc.n]
            lgk_calc = sN * (N + E)
            lgk_exp  = np.log10(self.rxns.k_exp[r])
            
            if self.rxns.index.nuc[r] in self.nuc.error:
                self.nuc.error[self.rxns.index.nuc[r]].append(lgk_exp - lgk_calc)
            if self.rxns.index.elec[r] in self.elec.error:
                self.elec.error[self.rxns.index.elec[r]].append(lgk_exp - lgk_calc)
                
        rm_idx = []
                
        for i in self.nuc.error:
            if len(self.nuc.error[i]) == 1 or (len(self.nuc.error[i]) == 2 and not i == self.nuc.anchor):
                rm_idx.append(i)
                
        [self.nuc.error.pop(i) for i in rm_idx]
             
        rm_idx = []
            
        for j in self.elec.error:
            if len(self.elec.error[j]) == 1:
                rm_idx.append(j)
        
        [self.elec.error.pop(j) for j in rm_idx]
            
    #----------------------------------------------------------------------------------------------------------#
    
    def set_weights(self, x):
        
        self.set_species_error(x)
    
        nuc_dof     = np.zeros(self.nuc.n)  # dof : degrees of freedom
        elec_dof    = np.zeros(self.elec.n)
        nuc_sigma2  = np.zeros(self.nuc.n)
        elec_sigma2 = np.zeros(self.elec.n)

        for i in range(self.nuc.n):
            if self.nuc.index[i] in self.nuc.error:
                
                if self.nuc.index[i] == self.nuc.anchor:
                    nofp = 1 # nofp : number of free parameters
                else:
                    nofp = 2
                                
                nuc_dof[i]    = len(self.nuc.error[self.nuc.index[i]]) - nofp
                nuc_sigma2[i] = np.var(self.nuc.error[self.nuc.index[i]], ddof=nofp)
                
        for j in range(self.elec.n):
            if self.elec.index[j] in self.elec.error:
                
                elec_dof[j]    = len(self.elec.error[self.elec.index[j]]) - 1
                elec_sigma2[j] = np.var(self.elec.error[self.elec.index[j]], ddof=1)
                
        self.rxns.weights = []
        
        for r in range(self.rxns.n):
            
            if self.rxns.status[r] != 'ok':
                continue
                
            idN = np.where(self.nuc.index == self.rxns.index.nuc[r])[0].item()
            idE = np.where(self.elec.index == self.rxns.index.elec[r])[0].item()
            
            if not self.rxns.index.nuc[r] in self.nuc.error and not self.rxns.index.elec[r] in self.elec.error:
                pass
            
            elif not self.rxns.index.nuc[r] in self.nuc.error:
                stf = student_t.ppf(.975, df=elec_dof[idE]) # stf : student t-factor
                self.rxns.weights.append(1 / (stf**2 * elec_sigma2[idE]))
                
            elif not self.rxns.index.elec[r] in self.elec.error:
                stf = student_t.ppf(.975, df=nuc_dof[idN])
                self.rxns.weights.append(1 / (stf**2 * nuc_sigma2[idN]))
                
            else:
                dof = self.welch_satterthwaite(nuc_sigma2[idN], elec_sigma2[idE], nuc_dof[idN], elec_dof[idE])
                stf = student_t.ppf(.975, df=dof)
                self.rxns.weights.append(1 / (stf**2 * (nuc_sigma2[idN] + elec_sigma2[idE])))
            
        self.rxns.weights  = np.array(self.rxns.weights)
        self.rxns.weights /= self.rxns.weights.sum()
        
    #----------------------------------------------------------------------------------------------------------#
    
    def welch_satterthwaite(self, sigma2_nuc, sigma2_elec, dof_nuc, dof_elec):
        
        c_nuc  = (dof_nuc  + 1)**(-1)
        c_elec = (dof_elec + 1)**(-1)
        
        numerator   = (c_nuc * sigma2_nuc + c_elec * sigma2_elec)**2
        denominator = c_nuc**2 * sigma2_nuc**2 / dof_nuc + c_elec**2 * sigma2_elec**2 / dof_elec
        
        return numerator / denominator
    
    #----------------------------------------------------------------------------------------------------------#
    
    def draw_sample(self, seed):
        
        rng = np.random.RandomState(seed)
        
        self.rxns.pop = np.diff(np.concatenate(([0.], 
                                                np.sort(rng.uniform(0., 1., self.count('ok') - 1)), 
                                                [1.])))
        
    #----------------------------------------------------------------------------------------------------------#

    def optimize(self, x0=None, weighting=False, fix_weights=False, bootstrapping=False, seed=None,
                 return_result=False, monitoring=False):

        if x0 is None:
            x0 = self.x0


        if (weighting is True and fix_weights is False) or \
            weighting is True and fix_weights is True and not hasattr(self.rxns, 'weights'):
            self.set_weights(x0)

        if bootstrapping is True:
            if seed is None:
                seed = np.random.RandomState().get_state()[1][1]
            if monitoring is True:
                print(seed, flush=True)
            self.draw_sample(seed)

        t_start = time.time()
        result  = basinhopping(lambda x: self.get_error(x,
                                                        weighting=weighting,
                                                        bootstrapping=bootstrapping
                                                       ),
                               x0,
                               niter=0,
                               seed=0
                              )
        t_end   = time.time()

        result.time  = t_end - t_start
        result.error = self.get_error(result.x, weighting=False)

        del result.lowest_optimization_result

        if weighting is True:
            result.weights = self.rxns.weights.copy()
        else:
            result.weights = None

        if bootstrapping is True:
            result.population = self.rxns.pop.copy()
            result.pop_seed   = seed
        else:
            result.population = None
            result.pop_seed   = None

        self.result.append(result)

        self.latest = self.result[-1]

        if return_result is True:
            return result

    #----------------------------------------------------------------------------------------------------------#

    def optimize_weighted(self):

        self.optimize()

        finished = False

        while not finished:

            error = self.latest.error

            self.optimize(x0=self.latest.x, weighting=True)

            if np.abs(np.log10(error / self.latest.error)) < 1e-6:
                finished = True

    #----------------------------------------------------------------------------------------------------------#

    def optimize_bootstrapped(self, n_samples, weighting=False, monitoring=False, parallel=False):

        if weighting is True:
            self.optimize_weighted()

        if parallel is True:

            cpus    = multiprocessing.cpu_count() - 1
            t_start = time.time()
            result  = Parallel(n_jobs=cpus)(delayed(self.optimize)(weighting=weighting,
                                                                   fix_weights=True,
                                                                   bootstrapping=True,
                                                                   seed=b,
                                                                   return_result=True,
                                                                   monitoring=monitoring
                                                                  ) for b in range(n_samples))
            t_end   = time.time()

            self.par           = Attributes()
            self.par.cpus      = cpus
            self.par.n_samples = n_samples
            self.par.time      = t_end - t_start

            [self.result.append(b) for b in result]

            self.latest = self.result[-1]

        else:

            for b in range(n_samples):

                self.optimize(weighting=weighting,
                              fix_weights=True,
                              bootstrapping=True,
                              seed=b
                             )
